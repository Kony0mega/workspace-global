import React from 'react'

function Spinner() {
    return (
        <div>
              <img
        src="https://icon-library.com/images/animated-svg-loading-icon/animated-svg-loading-icon-13.jpg"
        style={{ width: '340px', margin: 'auto', display: 'block' }}
        alt="Loading..."
      />
      <div style={{ display: 'block', width: 700, padding: 30 }}>
      <h4>React-Bootstrap Spinner Component</h4>
      With Border Animation: 
      <Spinner animation="border" variant="primary" /> <br/>
      With Grow Animation:
      <Spinner animation="grow" variant="warning" />
    </div>
        </div>
    )
}

export default Spinner
