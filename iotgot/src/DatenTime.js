import React from 'react'
import date from 'date-and-time'; 

 function DatenTime() {
    const now=new Date();
    const pattern =date.compile('ddd, MMM DD YYYY, hh:mm:ss A [GMT]Z',true)
    
    return (
        <div>
            <p>
                { date.format(now,pattern)}
            </p>
        </div>
    )
}
export default DatenTime;