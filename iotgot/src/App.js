
import Admin from './Admin';
import './App.css';
import Login from './Login';
import {BrowserRouter as Router,Switch,Route} from"react-router-dom"

import DeviceDisplay from './DeviceDisplay';
import 'bootstrap/dist/css/bootstrap.min.css';

//import Backdrops from './Backdrops';

  function App() {

  
  

  return (
       <Router>
         
    <div className="app">
   <Route path="/login"><Login/></Route>

     <Switch>

<Route path="/admin">
<Admin /> 
<DeviceDisplay /> 
</Route>

  </Switch>
     </div>
      </Router>
  )
}

export default App;
