import React, { useState } from 'react'
import PersonIcon from '@material-ui/icons/Person';
import SecurityIcon from '@material-ui/icons/Security';

import { Button } from 'react-bootstrap';
import './Login.css';
import { NavLink,useHistory } from 'react-router-dom';

import Backdrops from './Backdrops';






function Login() {
const [userName,setuserName]=useState('');
const [password,setpassword]=useState('');

const history=useHistory();


/*
const signIn = e => {
    e.preventDefault();

    auth
        .signInWithEmailAndPassword(userName, password)
        .then(auth => {
            history.push('/admin')
        })
        .catch(error => alert(error.message))
}

const register = e => {
    e.preventDefault();

    auth
        .createUserWithEmailAndPassword(userName, password)
        .then((auth) => {
            // it successfully created a new user with email and password
            if (auth) {
                history.push('/admin')
            }
        })
        .catch(error => alert(error.message))
}

*/

    return (
        <div className="main__login">
            
           
            <div className="main__container">
            
            
{/** image */}
<img className="main__image"src="https://upload.wikimedia.org/wikipedia/en/thumb/d/d0/Dogecoin_Logo.png/150px-Dogecoin_Logo.png " />
<div className="title__signin">
    <h2 >
    Sign in
</h2>
</div>

<form >
<div className="main__input">
<PersonIcon className="main__person-icon" />
<input placeholder="username" type="text" className="main__input-user" value={userName} onChange={e => setuserName(e.target.value)} required  />
</div>

<div className="main__password">
<SecurityIcon className="main__security-icon"/>
<input   placeholder="password" className="main__text-password" type="password" value={password} onChange={e =>setpassword(e.target.value)} required /> 
</div>

<div className="main__checkbox">
    
    <input type="checkbox" className="main__checkbox-input" />
  
    <label className="checkbox__label" >Remember me</label>
</div>

<div className="container__button">

     <Button className="main__button" href="/admin" > Login </Button>
     
     
     
   
</div>
</form>


{/** contianer */}
            </div>
{/** main div */}
        </div>
    )
}

export default Login



