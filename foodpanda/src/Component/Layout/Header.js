import {Fragment} from 'react';

import mealsImage from '../../assests/meals.jpg';
import classes from './Header.Module.css'
import HeaderCartButton from './HeaderCardButton';
const Header=(props)=>{

return <Fragment>
<header className={classes.header}>
    <h1>
        reactMeals
    </h1>
  <HeaderCartButton onClick={props.onShowCart} />
</header>
<div className={classes['main-image']}>
    <img  src={mealsImage} alt="image.png" />
</div>

</Fragment>

};

export default Header;