import React, {useEffect} from 'react';
import './App.css';
import Header from './Header';
import Home from './Home';
import Checkout from './Checkout';
import {BrowserRouter as Router,Switch,Route} from"react-router-dom"
import Login from './Login';
import { auth } from './fireBase';
import { useStateValue } from './StateProvider';
import Payment from './Payment';
import {loadStripe} from "@stripe/stripe-js"
import {Elements } from '@stripe/react-stripe-js'

const promise=loadStripe('pk_test_51HG4ZoB2lL2PrKLz9luD8eHAgJNvd6zVCLttRQYVV5DcWSGxzhNCxJxNoaBCQGSTOEkf8K14llXXcFYSN2wvkmaW00xnwTDvfq')

function App() {

const [{}, dispatch] = useStateValue()

useEffect(() => {
  auth.onAuthStateChanged(authUser =>{
    alert(auth);
    if(authUser){
     
      dispatch({
        type:'SET_USER',
user:authUser
      })
    }
    else
    {
      dispatch({
        type:'SET_USER',
        user:null
      })
    }
  })
}, [])

  return (
    <Router>
    <div className="app">
       <Switch>
       <Route path="/login">
<Login />
       </Route>

       <Route path="/checkout">
          < Header/> 
          <Checkout/>
          </Route>
          <Route path="/payment">
          < Header/> 
          <Elements stripe={promise} >
          <Payment />
          </ Elements>
          </Route>
       <Route path="/">
{/*      Header  nav bar */}


< Header/>
<Home />
{/*      products*/}

{/*      footer*/}
       </Route>
      
        </Switch>    
    </div>
    </Router>
  );
  
}

export default App;
