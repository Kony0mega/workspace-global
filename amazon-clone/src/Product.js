import React from 'react'
import './product.css'
import {useStateValue} from './StateProvider'
function Product({id,title,image,price,rating}) {
   
const [{basket},dispatch]=useStateValue();

   const addToBasket=()=>{
       //dispatch to data layer.
   dispatch({

    type:'ADD_TO_BASKET',
    item:{
        id:id,
        title:title,
        image:image,
        price:price,
        rating:rating
    },
   });
    };
   
   
    return (
        <div className="product">

{/** info */}
<div className="product__info">
<p>
{title}
</p>
<p className="product__price">
    <small> &#x20B9;</small>
    <strong>{price}</strong>
</p>
<div className="product__rating">
{Array(rating).fill().map((_,i)=>
    <p>&#11088;</p>
)}

</div>

</div>
<img className="" src={image} alt="img.png"/>

<button onClick={addToBasket}> Add To cart</button>


          {/** end of tag main */} 
        </div>
    )
}

export default Product
