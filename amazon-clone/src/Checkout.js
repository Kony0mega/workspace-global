import React from 'react'
import Subtotal from './Subtotal'
import './checkout.css'
import { useStateValue } from './StateProvider'
import CheckoutProduct from './CheckoutProduct';
function Checkout() {
    const [{basket,user},dispatch]=useStateValue();
    return (
        <div className="checkout">
            <div className="checkout__left">
               <img className="checkout__ad" src="https://images-eu.ssl-images-amazon.com/images/G/31/img21/CEPC/PD21/R01_Deals_revealed_1242x300.jpg"  alt="image.png" />

            <h1> </h1>
            <div><h1 className="checkout__title">
            {user?.email }
                hello how do you : </h1>
            
                {basket.map(item => (
            <CheckoutProduct
              id={item.id}
              title={item.title}
              image={item.image}
              price={item.price}
              rating={item.rating}
            />
          ))}

            
           
                          </div>
            </div>

            
            
            <div className="checkout__right"> 
{/** sub total */}
<h2> sub total</h2>
<Subtotal  />
            </div>
        </div>
    )
}

export default Checkout
