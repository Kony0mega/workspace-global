import { CardElement ,useElements, useStripe } from '@stripe/react-stripe-js';
import React, {useState,useEffect}from 'react'
import { Link,useHistory } from 'react-router-dom';
import CheckoutProduct from './CheckoutProduct';
import  './Payment.css'
import { useStateValue } from './StateProvider'
import Axios from './Axios';
import { getBasketTotal } from './reducer';
import CurrencyFormat from 'react-currency-format';

function Payment() {

    const history=useHistory();
    const[succeeded,setSucceeded]=useState();
   const[processing,setProcessing]=useState();
const[clientSecret,setClientSecret]=useState();
const stripe=useStripe();
const elements=useElements();

   const [{basket,user}]=useStateValue();

const [error,setError]=useState(null);
const [disabled,setDisabled]=useState(true);

   
useEffect(() => {
  
const getClientSecret=async()=>{
    const response= await Axios({
method:'post',
url:`payments/create?total=${getBasketTotal(basket)*100}`

    });

    setClientSecret(response.thousandSeparator.clientSecret)
}

}, [basket])



const handelSubmit=(e)=>{
    e.preventDefault();
    setProcessing(true)
    const paylaod=  stripe.confirmCardPayment(clientSecret,{
        payment_method:{
            card:elements.getElement(CardElement)
        }
    }).then(({paymentIntnet})=>{
        setSucceeded(true)
        setError(null)
        setProcessing(false)

        history.replace('/orders')
    })
    
}

const handelChange=(event)=>{

setDisabled(event.empty);
setError(event.error? event.error.message:"")

}

    return (
        <div className="payment">
            <div className="payment__container" >

<h1>
Checkout{<Link to=" /checkout"> {basket?.length } items </Link>}

</h1>
<div className="payment__section">
<div className="payment__title">
<h3>
    delivery Address
</h3>
<div className="payment__address" >
<p>
    {user?.email}
</p>
<p>#lane 1st</p>
<p># flat no.</p>
</div>

</div>
</div>
<div className="payment__section">
<div className="payment__title">
<h3>
    review items and delivery
</h3>
</div>
<div className="payment__items" >
{basket.map(item =>{
    <CheckoutProduct 
    id={item.id}
    title={item.title}
    image={item.image}
    price={item.price}
    rating={item.rating}/>

})}
</div>
</div>

<div className="payment__section">
{/** delivery paymnet*/}
<div className="payment__title" >
<h3>Payment method</h3>
</div>

<div className="payment__details"  >
    {/** stripe */}

<form onSubmit={handelSubmit}>
<CardElement onChange={handelChange}  />

<div className="payment__priceContainer" >
<CurrencyFormat
        renderText={(value) => (
          <h3>
              Order Total: {value}
          </h3>
        )}
        decimalScale={2}
        value={getBasketTotal(basket)} 
        displayType={"text"}
        thousandSeparator={true}
        prefix={"₹"}
      />
      <button disabled={processing || disabled || succeeded} >
          <span>
              {processing ? <p>processing</p>: "Buy Now"}
          </span>
      </button>
</div>


    </form>


</div>




            </div>








        </div>
        </div>
    )
}

export default Payment
