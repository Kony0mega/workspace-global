import React from 'react'
import  './home.css'
import Product from './Product'
function Home() {
    return (
        <div className="home">
<div  className="home__container">

<img  className="home__image"  src="https://images-eu.ssl-images-amazon.com/images/G/31/prime/Events/Pug/Leadup21-page/Eng-Mobile/1348517_in_prime_2_pd_1500x600_Eng._CB663273102_.jpg" alt="img.png"/>

<div className="home__row">
{/** products */}
<Product  id ="101" title='aghora I' price={450.00} image="https://images-eu.ssl-images-amazon.com/images/
I/510zSdnXAgL._SX198_BO1,204,203,200_QL40_FMwebp_.jpg" rating={5}   />



{/** products */}
<Product id ="102" title='aghora II' price={450.00} image="https://m.media-amazon.com/images/I/717CFG27AZL._AC_UY218_.jpg" rating={4}  />
</div>

<div className="home__row">
{/** products */}
<Product id="1003" title="Creative Farmer Exotic Plant Orange Rare Tasty Israeli 
Kumquat Orange Tropical Fruit Plant Plant (1 Healthy Live Plant)" price={399.00}  image=" https://images-na.ssl-images-amazon.com/images
/I/41ZhXRIjLUL.jpg" rating={3}  />
{/** products */}
<Product  id="108" title="Green Paradise Live Beautiful Pink Trumpet Tabebuia rosea Tree Sapling Plant" 
price={ 550.05}  image="https://images-na.ssl-images-amazon.com/images/
I/61hMc%2BEaM%2BL.jpg " rating={5} />
{/** products */}
<Product id="1009" title=" Indian Gardens - Adenium Arabicum big (live plant) - Set of 10 , pink colour" price={450.00}  image="https://m.media-amazon.com/images/I
/51FbvEYnQlL._AC_UL320_.jpg " rating={2}/>
</div>

<div className="home__row">
{/** products */}
<Product id="1011" title="Apple AirPods Pro" price={25000}  image="https://images-na.ssl-images-amazon.com/
images/I/71zny7BTRlL._SL1500_.jpg " rating={5} />
</div>

{/*container tag ends */}
</div>
{/*  ends tag   */}
        </div>
    )
}

export default Home
